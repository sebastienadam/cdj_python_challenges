# Python exercises

## PyPet

Original idea: https://www.thinkful.com/learn/intro-to-python-tutorial/Creating-Your-Pypet#Creating-Your-Pypet

In this exercise, you will create a virtual pet. You can give it different characteristics like a name, a type...

You're gonna have to take care of your pet. He's gonna ask you to do some things for him. If you do, he'll be happy. Otherwise, he'll be angry.

### Related techniques

* Showing text
* Using variables
* Using lists
* Asking information form user
* Conditional statements
* Using loops (`while` and `for`)
* Random choice in a list
