#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random
import time

# initialization
name = 'Fluffy'
photo = '(=^o.o^=)__'
talk = 'Meow'

actions = [
    'Play with me...',
    "I'm thirsty, give me something to drink",
    "I'm hungry, what's to eat?"
]

responses = [
    'Play with '+ name,
    'Give some drink',
    'Give some food',
    'Go to sleep!'
]

# main loop
print("Hi! I'm", name)
print(photo)
print(talk)
action = random.choice(actions)
while True:
    print()
    print(action)
    print('What are you gonna do?')
    for i in range(len(responses)):
        print(i + 1, responses[i])
    response = int(input('> '))
    if response == 4:
        print(talk)
        print("I'm going to sleep. Bye.")
        break
    if action == actions[0]:
        if response == 1:
            print(talk)
            print('I like to play with you :-)')
            time.sleep(1)
            action = random.choice(actions)
        else:
            print("I don't want to do that! :-(")
    elif action == actions[1]:
        if response == 2:
            print(talk)
            print('So good.. :-)')
            time.sleep(1)
            action = random.choice(actions)
        else:
            print("I don't want to do that! :-(")
    elif action == actions[2]:
        if response == 3:
            print(talk)
            print('Miam Miam... :-)')
            time.sleep(1)
            action = random.choice(actions)
        else:
            print("I don't want to do that! :-(")
