# Python exercises

[![Logo Python](https://www.python.org/static/community_logos/python-logo-generic.svg)](https://www.python.org/)
[![Logo CoderDojo Belgium](logo-cdj-be.png)](https://www.coderdojobelgium.be/)

Here are some exercises in [Python](https://www.python.org/). They are designed to teach programming to young people. They were designed for python 3.

Python documentation: https://docs.python.org/3/

## Exercises

Here is a list of the exercises and their related techniques:

1. [Guess the number](guess_the_number/)
    * Showing text
    * Using variables
    * Conditional statements
    * Random number
    * Asking information form user
    * Loop (`while`)
1. [Quiz](quiz/) This exercise has different levels of complexity:
    * Version 1 (`script_1.py`):
      * Showing text
      * Using variables
      * Conditional statements
      * Using nested lists
      * Random choice in a list
      * Asking information form user
    * Version 2 (`script_2.py`) - same as version 1 plus:
      * Reading text file
      * String to list
    * Version 3 (`script_3.py`) - same as version 1 plus:
      * Reading [JSON](https://json.org/) text file
      * Using dictionaries
1. [Magic 8-Ball](magic_8/) This exercise has different levels of complexity:
    * Version 1:
        * Showing text
        * Using variables
        * Conditional statements
        * Lists
        * Random choice in a list (optional)
        * Using loops (`while`)
    * Version 2 - same as version 1 plus:
        * Reading text file
        * [List comprehension](https://docs.python.org/3.6/tutorial/datastructures.html#list-comprehensions) (optional)
1. [PyPet](pypet/)
    * Showing text
    * Using variables
    * Using lists
    * Asking information form user
    * Conditional statements
    * Using loops (`while` and `for`)
    * Random choice in a list
1. [Langton's ant](langton_s_ant/) This exercise has different levels of complexity:
    * Version 1:
      * Showing text
      * Using variables
      * Conditional statements
      * Nested lists
      * Random choice in a list (optional)
      * Using loops (`while` and `for`)
    * Version 2 - same as version 1 plus:
      * [List comprehension](https://docs.python.org/3.6/tutorial/datastructures.html#list-comprehensions)
1. [Tic-Tac-Toe](tic_tac_toe/)
    * Showing text
    * Using variables
    * Conditional statements
    * Getting information from user
    * Nested lists
    * Join lists
    * Using loops (`while` and `for`)
    * [List comprehension](https://docs.python.org/3.6/tutorial/datastructures.html#list-comprehensions)
