# Python exercises

## Langton's ant

In this exercise, you will implement the Langton's ant (More info: https://en.wikipedia.org/wiki/Langton's_ant).

You're going to move an ant on a whiteboard. Each time the ants arrive on a white cell, it becomes black and the ants turn half a right. Each time the ant arrives on a black cell, it becomes white and the ant turns half a turn to the left. Then the ant moves one square forward and the process starts again.

The program must stop when the ant leaves the board.

The final result is surprising.

### Related techniques

There are two versions for this script with different levels of complexity:

1. Version 1 (`script_1.py`):
    * Showing text
    * Using variables
    * Conditional statements
    * Nested lists
    * Random choice in a list (optional)
    * Using loops (`while` and `for`)
2. Version 2 (`script_2.py`) - same as version 1 plus:
    * [List comprehension](https://docs.python.org/3.6/tutorial/datastructures.html#list-comprehensions)

### Possible enhancements

* Put more than one ant
* ...
