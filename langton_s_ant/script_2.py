#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random

nb_lines = 128
nb_cols = 128
moves = [(0,-1),(-1,0),(0,1),(1,0)]
# create the board
board = [[0 for i in range(nb_cols)] for j in range(nb_lines)]

# initial move
move_x, move_y = random.choice(moves)
#move_x, move_y = -1, 0

# initial ant position
#ant_x = random.choice(range(nb_lines))
#ant_y = random.choice(range(nb_cols))
ant_x = int(nb_lines / 2 - 1)
ant_y = int(nb_cols / 2 - 1)

# moving the ant
while True:
    if ant_x < 0 or ant_x >= nb_lines or ant_y < 0 or ant_y >= nb_cols:
        break
    if board[ant_x][ant_y] == 0:
        board[ant_x][ant_y] = 1
        move_x, move_y = moves[(moves.index((move_x, move_y))+1)%len(moves)]
    else:
        board[ant_x][ant_y] = 0
        move_x, move_y = moves[moves.index((move_x, move_y))-1]
    ant_x += move_x
    ant_y += move_y

# print the final solution
for x in range(len(board)):
    print(''.join(map(str, board[x])).replace('0','.').replace('1','X'))
