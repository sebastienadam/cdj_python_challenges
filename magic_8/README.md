# Python exercises

## Magic 8-Ball

![8-ball photo from wikipedia commons](https://upload.wikimedia.org/wikipedia/commons/7/78/8_ball_face.jpg)

In this challenge, you will implement the "Magic 8-Ball" game (more info: https://en.wikipedia.org/wiki/Magic_8-Ball).

Your program will ask the user for a question and it will give him (her) the answer (don't worry about the question, just give a random pre-defined answer ;-) ). Usually, this small program is very popular...

In the first version of the script, the answers are encoded in the program itself. In the second version, the answers are read from a text file.

### Related techniques

There are two versions for this script with different levels of complexity:

1. Version 1 (`script_1.py`):
    * Showing text
    * Using variables
    * Conditional statements
    * Lists
    * Random choice in a list (optional)
    * Using loops (`while`)
2. Version 2 (`script_2.py`) - same as version 1 plus:
    * Reading text file
    * List comprehension (optional)
