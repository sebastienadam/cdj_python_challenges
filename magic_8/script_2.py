#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random

with open('answers.txt','r',encoding='utf-8') as fin:
    ## Loading file in one piece and splitting on new line
    #answers = fin.read().split('\n')
    #answers.remove('')

    ## Loading using traditional 'for' loop
    answers = []
    for line in fin:
        answers.append(line.strip())

    ## Loading using list comprehension
    #answers = [line.strip() for line in fin]

while True:
    print("What's your question?")
    question = input('> ')
    if question == '':
        break
    print(random.choice(answers))
