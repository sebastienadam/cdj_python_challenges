#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# initialization
board_size = 3
players = ['X', 'O']
current_player = 0
## creating the board
board = [[' ' for cell in range(board_size)] for line in range(board_size)]

# main loop
for i in range(board_size**2):
    print('\n-+-+-\n'.join(['|'.join([cell for cell in line]) for line in board]))
    chosen = input('{} > '.format(players[current_player]))
    if chosen in ['', '0']:
        break
    chosen = int(chosen) - 1
    x, y = (chosen // 3,chosen % 3)
    board[x][y] = players[current_player]
    current_player = (current_player + 1) % 2
    print()
