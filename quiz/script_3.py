#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random
import json

with open('data.json', 'r', encoding= 'utf-8') as fin:
    q_a = json.load(fin)

item = random.choice(q_a)

print(item['question'])
reply = input("> ")
if reply.lower() == item['answer'].lower():
    print("Well done!")
else:
    print("Oops, the answer is:", item['answer'])
