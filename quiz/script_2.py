#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random

q_a = []

with open('data.txt', 'r', encoding='utf-8') as fin:
    for line in fin:
        q_a.append(line.strip().split('|'))

question, answer = random.choice(q_a)

print(question)
reply = input("> ")
if reply.lower() == answer.lower():
    print("Well done!")
else:
    print("Oops, the answer is:", answer)
