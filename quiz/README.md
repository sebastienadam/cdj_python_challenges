# Python exercises

## Quiz

In this challenge, you will create a script that will ask the user a question and request the answer. Then your program will indicate whether the answer is correct or not.

In the first version of the proposed script, questions and answers are encoded directly into the program.

In the second version, the questions and answers are in a text file. Each question/answer pair is on a line, separated by a special character. The line must then be divided to separate the questions from the answers.

In the third version, the questions and answers are in a JSON file. They can be easily imported into a dictionary.

### Related techniques

There are two versions for this script with different levels of complexity:

1. Version 1 (`script_1.py`):
    * Showing text
    * Using variables
    * Conditional statements
    * Using nested lists
    * Random choice in a list
    * Asking information form user
2. Version 2 (`script_2.py`) - same as version 1 plus:
    * Reading text file
    * String to list
3. Version 3 (`script_3.py`) - same as version 1 plus:
    * Reading [JSON](https://json.org/) text file
    * Using dictionaries

### Possible enhancements

* Ask more than one question
* Score
* ...

Translated with https://www.DeepL.com/Translator
