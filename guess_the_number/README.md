# Python exercises

## Guess the number

In this challenge, you will create a script chose a random number and ask the user to guess it. The program will have to indicate whether the number given by the user is too hight, too low or correct.

### Related techniques

* Showing text
* Using variables
* Conditional statements
* Random number
* Asking information form user
* Loop (`while`)

### Possible enhancements

* Showing number of tries
* Maximum tries
* Score
* ...
